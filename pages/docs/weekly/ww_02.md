---
Week: 7
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 7 - Dokumentationsteknik og grundlæggende netværk

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Læs undervisningsplanen og øvelser
- Læs kapitel 2 – Sårbarheder i informationssystemer i "IT-Sikkerhed i praksis", hav fokus på CIA modellen.

### Praktiske mål

- Øvelser gennemført og dokumenteret

### Øvelser

I skal lave THM Øvelserne fra jeres kali på VMWare Workstation.  
Husk at dokumentere i jeres gitlab projekt når i arbejder på øvelser.  
Øvelserne bør udføres i den rækkefølge som de står herunder.

1. [Øvelse 28 - CIA modellen](../exercises/28_intro_opgave_cia.md)
2. [Øvelse 2 - Git/gitlab og ssh nøgler](../exercises/2_intro_opgave_git_gitlab_ssh.md)
3. [Øvelse 1 - Markdown](../exercises/1_intro_opgave_markdown.md)
5. [Øvelse 98 - Dokumentation af opgaver](../exercises/98_intro_opgave_dokumentation.md)
6. [THM: Introductory networking](https://tryhackme.com/room/introtonetworking)
7. [THM: What is networking?](https://tryhackme.com/room/whatisnetworking)
8. [THM: Intro to LAN](https://tryhackme.com/room/introtolan)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for:_** 
    - Grundlæggende netværksprotokoller

- **_Den studerende kan supportere løsning af sikkerhedsarbejde ved at:_**  
    - Mestre forskellige netværksanalyse tools 

- **_Den studerende kan:_** 
    - ..

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende ved hvad OSI modellen og dens lag er
- Den studerende kender TCP/IP modellen, dens lag og hvordan 3 way handshake fungerer
- Den studerende kan anvende ping, hvilken protokol ping anvender og relevante ping parametre
- Den studerende ved hvad traceroute kan anvendes til og hvordan det bruges
- Den studerende ved hvad whois er og kan anvende det via CLI
- Den studerende har viden om hvordan DNS systemet er opbygget og hvordan dig kan bruges til at forespørge DNS servere
- Den studerende ved hvad et netværk er herunder hvad IP adresser og MAC adresser er
- Den studerende kender til MAC spoofing
- Den studerende kender de forskellige netværks topologier
- Den studerende ved hvad hhv. en switch og en router anvendes til
- Den studerende har viden om ARP protokollen
- Den studerende kender til DHCP og hvordan protokollen fungerer

Læringsmål fra studieordningen

## Afleveringer

- Øvelser dokumenteret på gitlab pages tak :-)

## Skema

Herunder er det planlagte program for dagen.

### Mandag

|  Tid  | Aktivitet                     |
| :---: | :---------------------------- |
| 8:15  | Introduktion til dagen        |
| 8:30  | CIA modellen                  |
| 9:30  | Øvelser K1/K4 (dokumentation) |
| 11:30 | Frokost                       |
| 12:15 | Øvelser K2/K3 (netværk)       |
| 15:30 | Fyraften                      |

## Kommentarer og links
