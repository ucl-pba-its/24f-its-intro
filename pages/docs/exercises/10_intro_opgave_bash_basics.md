# Øvelse 10 - bash scripting basics

## Information

Denne øvelse introducerer dig til bash og scripting i bash.  
Det er en forudsætning at du har adgang til en virtuel linux maskine. Gerne en kali linux kørende i f.eks vmware workstation pro.

## Instruktioner

1. Se denne korte video for at få et meget hurtigt indblik i bash [https://youtu.be/I4EWvMFj37g](https://youtu.be/I4EWvMFj37g)
2. Følg denne guide for at lære det fundamentale ved bash og bash scripting [https://linuxconfig.org/bash-scripting-tutorial-for-beginners](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)

## Links

En detaljeret gennemgang af opsætning og installation af kali linux på vmware er her: [https://www.kali.org/docs/virtualization/install-vmware-guest-vm/](https://www.kali.org/docs/virtualization/install-vmware-guest-vm/)

Hvis du vil køre kali på en anden host kan du se tilgængelige images her: [https://www.kali.org/get-kali/](https://www.kali.org/get-kali/)