# Øvelse 8 - Python recap

## Information

Programmering er noget i allesammen har stødt på før i forbindelse med grunduddannelsen.  
Arbejdet med sikkerhed kommer ofte i berøring med programmering, det kan være du i din karriere skal arbejde med at udvikle sikker software, eller samarbejde med udviklere, det kan være du skal evaluere scripts til netværks automation eller du skal analysere en suspekt fil som en medarbejder har modtaget på mail. 
Scripts bruges ofte i virksomheder til at automatisere forskellige processer, her er det også en fordel at kunne programmere.

Mange ting går igen i programmeringssprog, basis kontrol strukturer, datatyper, variabler, funktioner, klasser og objekter (+ meget andet)
Det kan være din programmerings baggrund er i C#, Java, C++, Python eller noget helt andet ?

Måske er det lang tid siden du har programmeret og denne øvelse kan du bruge som hjælptil at genopfriske din viden og evner indenfor programmering.  
Øvelsen her bruger Python fordi det er et ofte anvendt sprog i sikkerheds arbejdet. Det kan bruges til at lave små og specialiserede scripts der løser specifikke øvelser.

- Søgning i store tekst filer
- Visualisering af data
- Lave en hurtig http server
- Snifning af netværks trafik

Og meget andet...... 
Denne artikel belyser hvor i sikkerheds arbejdet du kan anvende python [https://www.botreetechnologies.com/blog/python-in-cybersecurity/](https://www.botreetechnologies.com/blog/python-in-cybersecurity/)

Hvis du ikke har programmeret i Python før burde det være muligt at overføre din viden fra andre programmeringssprog.

Python kræver ikke et stort og tungt IDE som visual studio eller pycharm, min erfaring er at det er bedre at bruge en teksteditor som visual studio code eller sublime tekst.  
Python har mange indbyggede moduler som kan klare de fleste daglige øvelser men udover det har Python et stort pakke system som hedder PIP, her findes moduler der kan klare de fleste øvelser og giver en masse funktionalitet hurtigt.  
Hvis du på et tidspunkt får brug for at bruge PIP pakker så vil jeg anbefale at du laver en sandkasse til din applikation, også kaldet et `Virtual environment`. Det sikrer at de pakker du installerer kun hører til din spplikation og ikke bliver installeret globalt.  
Læs mere om virtual environments her [https://realpython.com/python-virtual-environments-a-primer/](https://realpython.com/python-virtual-environments-a-primer/)


Øvelsen er en gruppe øvelse og i skal bruge nedenstående links som reference når i analyserer og programmerer.

Det kan ofte være en hjælp at skrive lidt pseudo kode eller lave et flow diagram når kode skal analyseres eller inden i begynder at programmere. 

Øvelsen tager udgangspunkt i dette simple Python lommeregner program på 60 linjer.

```py title="simple_calculator.py" linenums="1"
def add(num1, num2):
    return num1 + num2

def sub(num1, num2):
    return num1 - num2

def div(num1, num2):
    return num1 / num2

def mult(num1, num2):
    return num1 * num2

print('\nWelcome to the simple calculator (write q to quit)')

result = 0.0
chosen_operation = ''

while True:
    print('enter the first number')
    number1 = input('> ')
    if number1 == 'q':
        print('goodbye...')
        break
    print('enter the second number')
    number2 = input('> ')
    if number2 == 'q':
        print('goodbye...')
        break
    print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
    operation = input('> ')
    if operation == 'q':
        print('goodbye...')
        break

    try:
        number1 = float(number1)
        number2 = float(number2)
        operation = int(operation)

        if operation == 1:
            result = add(number1, number2)
            chosen_operation = ' added with '
        elif operation == 2:
            result = sub(number1, number2)
            chosen_operation = ' subtracted from '
        elif operation == 3:
            result = div(number1, number2)
            chosen_operation = ' divided with '
        elif operation == 4:
            result = mult(number1, number2)
            chosen_operation = ' multiplied with '

        print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
        print('restarting....')

    except ValueError:
        print('only numbers and "q" is accepted as input, please try again')

    except ZeroDivisionError:
        print('cannot divide by zero, please try again')
```

## Instruktioner

**Del 1**

1. Start med denne lille Python quiz: [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp)
2. Læs om hvad Python er [https://www.python.org/doc/essays/blurb/](https://www.python.org/doc/essays/blurb/) og kig på sammenligning med andre sprog [https://www.python.org/doc/essays/comparisons/](https://www.python.org/doc/essays/comparisons/)
2. Lav et fælles gitlab repo til det i laver i denne øvelse, klon det til jeres kali linux vm's.
3. Analyser sammen `simple_calculator.py`.  
   For hver linje i programmet noterer i hvad den enkelte linje gør. I må bruge alle hjælpemidler til at analysere programmet, husk at der er links herunder. Husk også at køre programmet så i dels ved at i kan afvikle python programmer, dels ved hvordan programmets funktionalitet er og at der ikke er fejl i programmet...  
   Læg mærke til datatyper, kontrol strukturer, variabler, funktioner, indentering osv.  
   Det er vigtigt at i snakker med hinanden og bruger hinandens erfaring med programmering, i en god dialog vil i også få snakket om forskellene på de forskellige sprog i kender.
4. På klassen laver vi code review ud fra jeres analayse, formålet er at alle har forstået koden og hvordan Python fungerer.

**Del 2**

Nu er det jeres tur til at programmere :-)

1. Omskriv så meget af programmet som muligt så det har så få gentagelser som muligt, altså abstraher så meget som muligt til funktioner.
2. Udvid lommeregner programmet så det kan logge input, resultater og fejl til en `.log` fil.  
Brug logging modulet i Python [https://realpython.com/python-logging/](https://realpython.com/python-logging/)
3. Omskriv programmet så regne funktionerne er i en klasse for sig selv og logging funktionalitet er i en anden klasse. Funktionaliteten skal være den samme som i punkt 5+6. 
4. Lav en python fil der importerer og bruger jeres klasser så programmet afvikles som det oprindelige program.

**Husk løbende at git committe og selvfølgelig at pushe når det giver mening.**

Vi gennemgår jeres programmer næste gang

## Links

- W3Schools Python reference [https://www.w3schools.com/python/default.asp](https://www.w3schools.com/python/default.asp)
- Officiel Python dokumentation [https://docs.python.org/3/reference/index.html](https://docs.python.org/3/reference/index.html)
- Gode Python tutorials [https://realpython.com/](https://realpython.com/)
- Gode Python videoer fra Socratica [https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-)
- Python basis kursus (bog, video, podcast) [https://www.py4e.com/](https://www.py4e.com/)