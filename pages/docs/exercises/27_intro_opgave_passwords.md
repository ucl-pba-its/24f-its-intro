# Øvelse 27 - Passwords og ressourcer

## Information

Der er ingen tvivl om at ressourcer er på dagsordenen. Ressourcer handler både om effektivisering men også om at der er begrænsede ressourcer til stede på planeten.  
Ressourcer er således både dyrebare for virksomheder og organisationer samt miljøet.  

Umiddelbart kan det være svært at koble IT sikkerhed og begreber som bæredygtighed og grøn omstilling men der er en sammenhæng.  
Vi har uanset hvad vi arbejder med et ansvar for at træffe beslutninger der tager hensyn til vores virksomheds brug af ressourcer samt mindske belastningen af miljøet.  
Det kan vi gøre i vores daglige arbejde ved at træffe beslutninger der inkluderer omtanke for miljøet. 

Denne øvelse tager udgangspunkt i virksomheders brug af passwords, altså hvilken password politik de har vedtaget og implementeret.  
En password politik rammer alle brugere af en virksomheds it systemer og bruger derfor af virksomhedens ressourcer.  
Password politik inkluder blandt andet:

- Password minimums længde
- Password kompleksitet
- Hvilke ord der må bruges
- Hvor ofte password skal skiftes

Hvis vi tager UCL som eksempel har de defineret en password politik for både ansatte og studerende, den kan du se her:  
[https://www.ucl.dk/teaching/vejledninger/guides](https://www.ucl.dk/teaching/vejledninger/guides)
Password må heller ikke være et tidligere, af brugeren, brugt password

Ansatte på UCL har et password der udløber hver 3.  
Hvor ofte studerende skal skifte har jeg ikke fundet noget information om, men det kan i prøve at finde ud af.

UCL har i runde tal omkring 1000 ansatte samt 10000 fuldtids studerende.  
Ud fra UCL's password politik er det tydeligt at der bruges en del ressourcer på at vedligeholde passwords.  
Hvis hver medarbejder bruger i gennemsnit 3 minutter på at skifte password 4 gange om året så betyder det at der bruges omkring 200 timer pr. år.Udover det skal lægges den tid IT personale bruger på at supportere og vedligeholde brugere og systemer der understøtter passwords.

Formålet med en password politik er naturligt nok at passwords er så sikre som muligt, men hvad der opfattes som et sikkert password har ændret sig en del igennem tiden.  

Spørgsmålet er om de ressourcer der lægges i den nuværende password politik så giver den bedste sikkerhed ?

Øvelsen er en gruppe øvelse. 

## Instruktioner

1. Undersøg hvor gammel den nuværende password politik hos UCL er (kig efter gamle microsoft password anbefalinger)
2. Debatter om hvilke konsekvenser IT politikker har for miljøet og virksomheder ressource forbrug.
3. Snak om andre områder indenfor IT som i kan påvirke i forhold til miljør og ressourcer.
4. Undersøg hvad anbefalingerne til password politik er i 2022. 
Tag udgangspunkt i [https://learn.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations](https://learn.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations) og find selv flere kilder.
5. Nævner ISO27000 eller andre standarder noget om password politik ?
6. Skriv et forslag til en opdateret miljø og ressource besparende password politik (i må gerne tænke alternativt i f.eks yubikey eller MFA) og giv jeres bedste bud på hvad det kan spare miljøet og UCL for i ressourcer.

## Links
