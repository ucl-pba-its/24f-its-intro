# Øvelse 16 - Opsætning af virtuelle maskiner i vmware

### Information

I denne øvelse skal du opsætte 2 virtuelle maskiner i vmware. Maskinerne skal bruge xubuntu 18.04 LTS som image.

![vrsx_intro_til_itsik.png](vrsx_intro_til_itsik.png)  
*Netværksdiagram*

### Instruktioner

1. Lav 2 xubuntu VM maskiner i vmware, brug xubuntu 20.04 LTS som image.
   - Xubuntu image download [https://xubuntu.org/download/](https://xubuntu.org/download/)
   - hjælp til at installere en ny vm [https://kb.vmware.com/s/article/1018415](https://kb.vmware.com/s/article/1018415)
2. Konfigurer de 2 linux maskiner's netværk ifølge netværks diagrammet
3. åbn vm indstillinger for hver xubuntu maskine. Sæt xubuntu 1 til vmnet1 og xubuntu 2 til vmnet2 (hvis du ikke har vmnet2 så lav det i vmware virtual network editor)

## Links
