# Øvelse 17 - Opsætning af vsrx virtuel router

### Information

I denne øvelse skal du opsætte en virtuel juniper vsrx (juniper srx240)

![vrsx_intro_til_itsik.png](vrsx_intro_til_itsik.png)  
*Netværksdiagram*

### Instruktioner

1. download vm filer fra itslearning
2. åben ovf fil i vmware workstation
3. giv vsrx vm et navn
4. tryk import
5. åbn indstillinger og forbind ge-0/0/1 til vmnet1 og ge-0/0/2 til vmnet2 (hvis du ikke har vmnet2 så lav det )  
   adapter 1 er interface ge-0/0/0, adapter2 er ge-0/0/1, adapter3 er ge-0/0/2 (se netværks diagram)
6. konfigurer vmnet 1 og 2 i virtual network editor. Kun _host only_ skal være valgt. Klik apply og check settings.

### Links

Per fra IT-Teknolog har lavet en video om hvordan det gøres, det kan evt. være en hjælp [https://youtu.be/SlPj1QYHzlM](https://youtu.be/SlPj1QYHzlM)