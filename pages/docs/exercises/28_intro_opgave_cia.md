# Øvelse 28 - CIA modellen

## Information

CIA modellen er central i arbejdet med informationssikkerhed.  
Hvor modellen stammer fra er der uenighed om, men modellen er simpel og kan bruges mange steder i arbejdet med sikkerhed.

På dansk er modellen oversat til FIT, men det er mest udbredt at anvende den engelske betegnelse.

C står for **C**onfidentiality (**F**ortrolighed) 
I står for **I**tegrity (**I**tegritet)
A står for **A**vailability (**T**ilgængelighed)

Formålet med denne øvelse er at blive bekendt med CIA modellen.

## Instruktioner

Øvelsen skal laves som gruppe/team

1. Læs om CIA modellen i bogen ["IT-Sikkerhed i praksis, en introduktion"](https://samfundslitteratur.dk/bog/it-sikkerhed-i-praksis) kapitel 2.
2. Vælg **et** af følgende scenarier som i vil vurdere i forhold til CIA:
    - En password manager (software)
    - En lægeklinik (virksomhed)
    - Dine data på computere og cloud (...)
    - Energileverandør (kritisk infrastruktur)
3. Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.
4. Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.) 
5. Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.) 
6. Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.


## Links

- CIA model [https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f](https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f)
- CIA & DAD model [https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee](https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee)