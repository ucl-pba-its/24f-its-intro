# Øvelse 4 - Studieordning og læringsmål

## Information

For at være sikker på at du kender uddannelsens grundlag, læringsmål, eksamener etc. skal du kende indholdet i de mest vigtige dokumenter der hører til uddannelsen.
Du kan finde uddannelsens dokumenter på [https://www.ucl.dk/uddannelser/it-sikkerhed#uddannelsens+opbygning](https://www.ucl.dk/uddannelser/it-sikkerhed#uddannelsens+opbygning)  
Nogen af dokumenterne kan findes ved at klikke på "Øvrige studiedokumenter"

## Instruktioner

1. Dan dig et overblik over hvad der står i "Studieordning National", hvad indeholder den og hvad står under de forskellige sektioner. Noter de spørgsmål du har til den nationale studieordning. _10 minutter_
2. Dan dig et overblik over hvad der står i "Studieordning Institutionel del 2022 (først gældende fra 1.feb)", hvad indeholder den og hvad står under de forskellige sektioner. Noter de spørgsmål du har til den Institutionel del af studieordningen. _10 minutter_
3. Læs læringsmålene for "Introduktion til IT Sikkerhed" i studieordningen - Noter de spørgsmål du har til læringsmålene. _10 minutter_
4. Læs om "Prøve i Introduktion til IT-sikkerhed", Noter spørgsmål til indholdet. _5 minutter_
5. Opsamling på klassen baseret på jeres spørgsmål. _10 minutter_

## Links
