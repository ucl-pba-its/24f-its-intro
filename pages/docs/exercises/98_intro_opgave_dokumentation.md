---
hide:
  - footer
---

# Øvelse 98 - Dokumentation af øvelser

## Information

Når du arbejder med øvelser bør der altid udarbejdes dokumentation af dit/jeres arbejde.  

Din dokumentation bør være offentlig tilgængelig, vi vil gerne have at det gøres på nettet via gitlab pages.  

At dokumentere øvelser tjener flere formål:

- At være i stand til at dokumentere hvad du har gjort hjælper med at fastholde din viden, færdigheder og kompetencer.
- Din dokumentation vil hjælpe dig når du skal involvere andre i dit arbejde, det kan være i forhold til at søge hjælp, men også når andre skal arbejde videre med det du har lavet.
- Du undgår at skulle "opfinde den dybe tallerken" igen.
- Din dokumentation vil være en god hjælp når du skal forberede dig til eksamen.
- Dokumentationen gør dig i stand til at vise andre (virksomheder, kollegaer) hvad du har arbejdet med.
- IT sikkerheds branchen har krav til dokumentation fra myndigheder og standardiserings organisationer hvilket gør evnen til at lave god dokumentation til en kerne kompetence for it sikkerheds profesionelle.

Der findes foskellige måder at dokumentere. Form og indhold kan afhænge af både øvelsen, men også hvem der er målgruppen.  
Det vigtigeste er at du forholder dig til at andre skal kunne anvende din dokumentation.  

Som minimum vil dokumentation af en øvelse indeholde:  

1. Beskrivelse af øvelsen
2. Hvordan kan andre reproducere det du har gjort for at udføre øvelsen, trin for trin guides er ofte gode til det.
3. Hvilke problemer havde du i udførelsen og hvad gjorde du for at løse dem.
4. Hvilke ressourcer (links og andre kilder) har du anvendt
5. Opsummering af tilegnede erfaringer 

## Instruktioner

1. Lav et markdown dokument, i dit gtilab dokumentations projekt, du kan bruge som skabelon til dokumentation af øvelser.
2. Dokumentér hvordan du har løst denne øvelse, brug din skabelon og push det til gitlab.
3. Send linket til en på dit team og spørg om personen vil vurdere og give feedback på din dokumentation.

## Links

- [Øvelse 1 - Markdown](../exercises/1_intro_opgave_markdown.md)
- [Øvelse 2 - Gitlab og ssh nøgler](../exercises/2_intro_opgave_git_gitlab_ssh.md)
- [Øvelse 3 - Gitlab pages](../exercises/3_intro_opgave_gitlab_pages.md)

Herunder er et par eksempler der kan give inspiration til forskellige måder at dokumentere på:  

- Trusselsmodelligerings manifestet [https://www.threatmodelingmanifesto.org/](https://www.threatmodelingmanifesto.org/)
- Offensive security red team report [https://redteam.guide/docs/Templates/report_template](https://redteam.guide/docs/Templates/report_template)
- Gitlab bug report [https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/issue_templates/Bug.md](https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/issue_templates/Bug.md)