# Øvelse 14 - Powershell basics

## Information

For at lære powershell at kende er her en Microsoft learning path som du kan gennemføre. Du skal oprette en azure konto for at bruge deres virtuelle maskiner. Alternativt kan du bruge f.eks. Kali Linux.  
Når du har gennemført og samtidig har kendskab til bash og python bør du være i stand til at skrive små powershell scripts ved at bruge yderligere dokumentation som f.eks den officielle dokumentation fra Microsoft.

## Instruktioner

1. Lav så meget som muligt af _Automate administrative tasks by using PowerShell_ [https://docs.microsoft.com/en-us/learn/paths/powershell/](https://docs.microsoft.com/en-us/learn/paths/powershell/)
2. Dan dig et overblik over hvad du kan finde i den officielle dokumentation [https://docs.microsoft.com/en-us/powershell/scripting/how-to-use-docs?view=powershell-7.2](https://docs.microsoft.com/en-us/powershell/scripting/how-to-use-docs?view=powershell-7.2)

## Links
