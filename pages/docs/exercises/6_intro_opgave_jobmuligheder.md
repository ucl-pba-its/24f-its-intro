# Øvelse 6 - Jobmuligheder

## Information

Denne øvelse er ment som en hjælp til at få et overblik over hvilke titler der er relevante for dig at søge beskæftigelse (job/praktikplads) indenfor.  
Idéen er også at du får oprettet en søge agent på en eller flere job databaser så du løbende holder øje med praktik og karriere muligheder.

## Instruktioner

1. Undersøg hvilke jobdatabaser der er relevante for it området. Som minimum synes jeg du skal bruge [https://www.jobindex.dk](https://www.jobindex.dk) og [https://www.it-jobbank.dk](https://www.it-jobbank.dk)
2. Lav søgninger i de jobdatabaser du har fundet i punkt 1. Find frem til de søgeord der giver flest relevante resultater.  
   Ved at søge samlet på `it-sikkerhed`, `informationssikkerhed` og `cybersecurity` fandt jeg på jobindex 146 stillinger.  
   På it-jobbank fandt jeg 44 stillinger.
3. Opret en job/søgeagent på dine valgte jobdatabaser så du løbende modtager information om stillinger og jobmuligheder.  
   Husk at søge bredt, både geografisk og i forhold til brancher, gå efter den søgning der giver dig flest resultater. Formålet er at få overblik, senere på uddannelsen, når du ved hvad du skal søge efter, kan du lave mere præcise søgeagenter.
4. Lav på klassen et samlet og delt markdown dokument hvor i lister jeres jobdatabaser og søgekriterier. Del dokumentet på klassen og med mig.
5. Som afslutning på øvelsen tager vi en runde på klassen hvor i præsenterer hvilke jobdatabaser i har fundet og hvilke søgekriterier i har udvalgt.

## Links
