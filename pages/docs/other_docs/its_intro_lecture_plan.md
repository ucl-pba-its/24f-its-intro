---
title: '24F PBa IT sikkerhed'
subtitle: 'Fagplan for Introduktion til IT sikkerhed'
filename: '24F_ITS1_INTRO_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for Introduktion til IT sikkerhed
skip-toc: false
semester: 24F
---

# Lektionsplan

| Underviser og indhold                                  | Uge | Emner                                                                                                                                                                                                                           |
| :----------------------------------------------------- | :-- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| NISI, Introduktion til faget og opsætning af værktøjer | 06  | It-kriminalitet og trusler fra cyberspace, Studerendes forskellige forudsætninger, VMWare workstation, Kali Linux på VMWare workstation, TryHackMe VPN adgang fra Kali                                                          |
| NISI, Git, grundlæggende netværk                       | 07  | Sårbarheder i informationssystemer, Git, gitlab, CIA modellen, switch, router, OSI, TCP/IP, ping, DNS, IP, MAC, ARP                                                                                                             |
| NISI, Sikkerhed i netværksprotokoller                  | 08  | Beskyttelse af it-systemer, Wireshark, opsætning af vsrx router og vm's i vmware workstation, NMAP scanning                                                                                                                     |
| NISI, Netværksanalyse                                  | 09  | Hacking og penetration testing med Kali Linux, Analyse af logfiler og netværkstrafik med forskellige værktøjer, udvidelse af virtuelt netværk med Damn Vulnerable Web Application                                               |
| NISI, Scripting - Bash og powershell                   | 10  | Praktisk kryptografi. Læse, forstå, afvikle samt rette i bash og powershell scripts                                                                                                                                             |
| NISI, Python programmering                             | 11  | Udvikling af sikker software, Skrive simple Python scripts, læse og analysere andres Python scripts                                                                                                                             |
| NISI, Programmer der kan bruge netværk                 | 12  | Information Security Management Systems (ISMS), Grundlæggende programmeringsprincipper med Socket, Urllib, HTML parsing. Anvende primitive datatyper og abstrakte datatyper, Konstruere simple programmer der kan bruge netværk |
| NISI, Programmer der kan bruge SQL databaser           | 15  | Etik og IT sikkerhed, Python og sqlite3, SQL injections, sqlmap                                                                                                                                                                 |
| NISI, Repetition                                       | 16  | Eksamen, recap og forberedelse                                                                                                                                                                                                  |
| NISI, Eksamen                                          | 17  | Eksamen baseret på fagets læringsmål.                                                                                                                                                                                           |

## Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt